export function textFromToken(x) {
  return x.toLowerCase()
}

export function textToTag(x) {
  let tag = { text: x }
  return tag
}

export function textFromTag(x) {
  return x.text
}

export function hasObject(objArr, obj) {
  let keys = Object.keys(obj)
  for (let i = 0; i < objArr.length; i++) {

    let match = keys.every(k => {
      if (k in objArr[i]) return objArr[i][k] === obj[k]
      return false
    })

    if (match) return i
  }
  return -1
}


export function loopIndex(val, min=0, max=Infinity) {
  // loop to top
  val = val > max ? min : val
  // loop to bottom
  val = val < min ? max : val
  return val
}
