/*

Definitions:

Tag: an object with form

  {
    text (required): <string>,
    type (optional): <string
  }

Text: the string which is used to compare user input to

Token: either a string or object from which a string can be extracted from.
The function textFromToken (default x => x) returns the value which is used to
compare user input to.

*/
import axios from 'axios'
import {textFromToken, textToTag, textFromTag, hasObject, loopIndex} from './utils.js';


/**
 * STATE:
 * activeSuggestion (int):
       the 0-indexed position of the highlighted suggestion which will be used on confirmation
 * data (array):
       the array of strings / objects in which to look
 * isLoading (boolean):
       whether or not data is being fetched or processed in some form or rashion
 * maxTags (int):
       how many tags the user can enter. default is Infintiy
 * recommend (int):
       how many suggestions to provide. default is 5.
 * suggestions (array):
       list of current suggestions
 * tagged (array):
       list of values confirmed by user and currently tagged
 * textFromTag (function):
      the inverse of textToTag. Retrieves user input from tag format.
 * textFromToken (function):
      how to convert an element of <data> to a string for comparision to user input.
      default is x => x.toLowerCase()
 * textToTag (function):
       how to convert input to tag format. default x => {text: x}
 */

const state = () => {
  return {
    activeSuggestion: 0,
    data: [],
    input: '',
    isLoading: false,
    log: [],
    maxTags: Infinity,
    recommend: 5,
    suggestions: [],
    tagged: [],
    textFromTag: textFromTag,
    textFromToken: textFromToken,
    textToTag: textToTag,
  }
}


const getters = {

}

const actions = {
  update({commit}, {vn, val}) {
    commit('set', {vn, val})
  },

  clearSuggestions({commit}) {
    commit('set', {vn: 'suggestions',      val: []})
    commit('set', {vn: 'activeSuggestion', val: 0 })
  },

  async fetchData({getters, commit}, lookup) {
    commit('set', {vn: 'isLoading', val: true})
    if (typeof lookup === 'string') {
      const listPromise =  axios.get(lookup)
      const [response] =  await Promise.all([listPromise])
      commit('set', {vn: 'data', val: response.data})
    } else {
      commit('set', {vn: 'data', val: lookup})
    }
    commit('set', {vn: 'isLoading', val: false})
  },

  search({getters, commit, state, dispatch}, text) {
    dispatch('clearSuggestions')
    // option to pass in text, or use state
    if (!text) text = state.input
    // empty string means nothing to search for
    if (text === '') return

    // transform to and from tag for complex tagging
    let putativeTag = state.textToTag(text);
    let compareText = state.textFromTag(putativeTag);

    // too many tags, nothing to search for
    if (!(state.tagged.length < state.maxTags)) return

    // flag processing, as searching may take a while
    commit('set', {vn: 'isLoading', val: true})
    let find = compareText.toLowerCase()
    for (let index = 0; index < state.data.length; index++) {
      // current token to check
      let maybe = state.textFromToken(state.data[index])
      // check for match
      let found = maybe.toLowerCase().indexOf(find.toLowerCase()) > -1
      // too many suggestions, no need to keep searching
      if (state.suggestions.length >= state.recommend) break
      if (found) {
        commit('pushSuggestion', state.textToTag(state.data[index]))
        commit('sortSuggestions', find)
      }
    }

    // all done here
    commit('set', {vn: 'isLoading', val: false})
  },

  incrementActiveSuggestion({state, commit}, increment) {
    let updated = state.activeSuggestion + increment
    updated = loopIndex(updated, 0, state.suggestions.length - 1)
    commit('set', {vn: 'activeSuggestion', val: updated})
  },






  // ADD (call PUSHERS)
  addTag({commit, state, dispatch}, tag) {
    if (state.tagged.length < state.maxTags) {
      commit('pushTag', tag)
    } else {
      commit('pushLog', {
        head: 'info',
        text: `only ${state.maxTags} allowed.`,
        type: 'warning',
      })
    }

    commit('set', {vn: 'input', val: ''})
    dispatch('clearSuggestions')
  },

  // REMOVE / DELETE (call SPLICERS)
  deleteTag({commit, state}, index) {
    if (index === -1) {
      index = state.tagged.length - 1
    }
    let tag = state.tagged[index]
    commit('spliceTag', tag)
  },
  removeTag({commit}, tag) {
    commit('spliceTag', tag)
  },


  confirmSuggestion({commit, state, dispatch}) {
    let sug = state.suggestions[state.activeSuggestion]
    commit('pushTag', sug)
    commit('set', {vn: 'input', val: ''})
    dispatch('clearSuggestions')
  },

}

const mutations = {
  // SETTERS
  set(state, {vn, val}) {
    state[vn] = val
  },


  // PUSHERS
  pushLog(state, log) {
    state.log.push(log)
  },

  pushTag(state, tag) {
    // only add tag if not already tagged
    if (hasObject(state.tagged, tag) > -1) return
    state.tagged.push(tag)
    state.suggestions = []
    state.activeSuggestion = 0
  },

  pushSuggestion(state, suggestion) {
    // only add suggestion if it is not tagged and if it is not already in suggestions

    if (hasObject(state.suggestions, suggestion) > -1) return
    if (hasObject(state.tagged, suggestion) > -1) return

    state.suggestions.push(suggestion)
  },

  // SPLICERS
  spliceLog(state, log) {
    let i = state.log.indexOf(log)
    state.log.splice(i, 1)
  },
  spliceTag(state, tag) {
    let i = state.tagged.indexOf(tag)
    state.tagged.splice(i, 1)
  },

  // SORTERS
  sortSuggestions(state, value) {
    let v = value.toLowerCase();
    state.suggestions.sort(
      (a, b) => state.textFromTag(a).toLowerCase().indexOf(v) - state.textFromTag(b).toLowerCase().indexOf(v)
    )
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
