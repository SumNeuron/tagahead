import Suggestion from './Suggestion.vue'
import Tag from './Tag.vue'
import Tagahead from './Tagahead.vue'
import Typeahead from './Typeahead.vue'


export default {
  Suggestion,
  Tag,
  Tagahead,
  Typeahead
}
