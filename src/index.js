import store from './store/module.js';
import * as utils from './store/utils.js';

// Import vue components
import components from './components/index.js'

// install function executed by Vue.use()
function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.keys(components).forEach((componentName) => {
    Vue.component(componentName, components[componentName]);
  });
}


// Create module definition for Vue.use()
const plugin = {
  install,
};

// To auto-install when vue is found
/* global window */
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// To allow use as module (npm/webpack/etc.) export component
// export default components;

let tagahead = {
  store,
  utils,
  ...components
}

export default tagahead
