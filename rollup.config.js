import vue from 'rollup-plugin-vue';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';
import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/index.js',
  extend: true,
  output: {
    name: pkg.name,
    exports: 'named',
  },
  external: ['axios'],
  globals: {
    axios: 'axios'
  },
  plugins: [
    vue({
      css: true,
      compileTemplate: true,
    }),
    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
      "runtimeHelpers": true,
      plugins: [

        [
          'wildcard',
          {
            exts: ['vue'],
            nostrip: true,
          },
        ],
        '@babel/plugin-external-helpers',
        '@babel/plugin-transform-runtime',
      ],
      presets: [
        [
          '@babel/preset-env',
          {
            modules: false,
          },
        ],
      ],
    }),
  ],
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
